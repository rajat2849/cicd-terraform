#!/bin/bash
sudo yum update -y
sudo yum install docker -y
sudo usermod -a -G docker ec2-user
sudo chmod 777 /var/run/docker.sock
sudo systemctl start docker
sudo chmod 777 /var/run/docker.sock
